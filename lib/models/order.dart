import 'package:json_annotation/json_annotation.dart';

part 'order.g.dart';

@JsonSerializable()
class Order {
  final String? ref;
  final int creationDate;
  final int deliverDay;
  final int deliverHour;
  final int deliverMonth;
  final String? menuRef;
  final String? paymentRef;

  Order(this.ref, this.creationDate, this.deliverDay, this.deliverHour, this.deliverMonth, this.menuRef, this.paymentRef);

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  Map<String, dynamic> toJson() => _$OrderToJson(this);
}
