// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Menu _$MenuFromJson(Map<String, dynamic> json) => Menu(
      ref: json['ref'] as String?,
      name: json['name'] as String,
      description: json['description'] as String,
      photoURL: json['photoURL'] as String,
      price: json['price'] as int,
      returnPolicy: json['returnPolicy'] as String,
    );

Map<String, dynamic> _$MenuToJson(Menu instance) => <String, dynamic>{
      'ref': instance.ref,
      'name': instance.name,
      'description': instance.description,
      'photoURL': instance.photoURL,
      'price': instance.price,
      'returnPolicy': instance.returnPolicy,
    };
