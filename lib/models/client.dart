import 'package:json_annotation/json_annotation.dart';

part 'client.g.dart';

@JsonSerializable()
class Client {
  final String? ref;
  final String? birthday;
  final String status;
  final List<String>? ordersRefs;
  final List<String>? methodsRefs;
  final List<String>? likedMenusRefs;

  Client(
      {this.ref,
      this.birthday,
      required this.status,
      this.ordersRefs,
      this.methodsRefs,
      this.likedMenusRefs});

  factory Client.fromJson(Map<String, dynamic> json) => _$ClientFromJson(json);

  Map<String, dynamic> toJson() => _$ClientToJson(this);
}
