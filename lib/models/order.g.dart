// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) => Order(
      json['ref'] as String?,
      json['creationDate'] as int,
      json['deliverDay'] as int,
      json['deliverHour'] as int,
      json['deliverMonth'] as int,
      json['menuRef'] as String?,
      json['paymentRef'] as String?,
    );

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'ref': instance.ref,
      'creationDate': instance.creationDate,
      'deliverDay': instance.deliverDay,
      'deliverHour': instance.deliverHour,
      'deliverMonth': instance.deliverMonth,
      'menuRef': instance.menuRef,
      'paymentRef': instance.paymentRef,
    };
