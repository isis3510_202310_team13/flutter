import 'package:json_annotation/json_annotation.dart';

part 'menu.g.dart';

@JsonSerializable()
class Menu {
  final String? ref;
  final String name;
  final String description;
  final String photoURL;
  final int price;
  final String returnPolicy;

  Menu(
      {this.ref,
      required this.name,
      required this.description,
      required this.photoURL,
      required this.price,
      required this.returnPolicy});

  factory Menu.fromJson(Map<String, dynamic> json) => _$MenuFromJson(json);

  Map<String, dynamic> toJson() => _$MenuToJson(this);
}
