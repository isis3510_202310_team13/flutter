// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Client _$ClientFromJson(Map<String, dynamic> json) => Client(
      ref: json['ref'] as String?,
      birthday: json['birthday'] as String?,
      status: json['status'] as String,
      ordersRefs: (json['ordersRefs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      methodsRefs: (json['methodsRefs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      likedMenusRefs: (json['likedMenusRefs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$ClientToJson(Client instance) => <String, dynamic>{
      'ref': instance.ref,
      'birthday': instance.birthday,
      'status': instance.status,
      'ordersRefs': instance.ordersRefs,
      'methodsRefs': instance.methodsRefs,
      'likedMenusRefs': instance.likedMenusRefs,
    };
