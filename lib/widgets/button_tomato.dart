import 'package:flutter/material.dart';
import 'package:project/theme/ui_constants.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonTomato extends StatelessWidget {
  final String text;
  final Function()? onPressed;
  const ButtonTomato({Key? key, required this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return MaterialButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        disabledColor: Colors.orange[300],
        elevation: 0,
        color: kTomato,
        onPressed: onPressed,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 25),
          width: size.width * 0.8,
          child: Align(
            alignment: Alignment.center,
            child: Text(text,
                style: GoogleFonts.montserrat(
                    color: kWhite, fontSize: 16, fontWeight: FontWeight.w600)),
          ),
        ));
  }
}
