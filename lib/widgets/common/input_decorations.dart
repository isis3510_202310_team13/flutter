import 'package:flutter/material.dart';

class InputDecorations {
  static InputDecoration authInputDecorations(
      {String? hintText, required String labelText, IconData? prefixIcon}) {
    return InputDecoration(
        // enabledBorder: const UnderlineInputBorder(
        //     borderSide: BorderSide(color: Colors.deepPurpleAccent)),
        // focusedBorder: const UnderlineInputBorder(
        //     borderSide: BorderSide(color: Colors.deepPurple, width: 2)),
        hintText: hintText,
        labelText: labelText,
        labelStyle: const TextStyle(fontWeight: FontWeight.w800),
        prefixIcon:
            prefixIcon != null ? Icon(prefixIcon, color: Colors.amber) : null);
  }
}
