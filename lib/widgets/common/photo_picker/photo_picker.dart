import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project/widgets/common/photo_picker/view_model.dart';
import 'package:provider/provider.dart';

class OnboardingPhotoPicker extends StatefulWidget {
  const OnboardingPhotoPicker({super.key});

  @override
  OnboardingPhotoPickerState createState() => OnboardingPhotoPickerState();
}

class OnboardingPhotoPickerState extends State<OnboardingPhotoPicker> {
  final ImagePicker _picker = ImagePicker();

  ImageSource? _source;

  Future<void> _showImageSourceOptions(BuildContext context) {
    final vm = context.read<PhotoPickerViewModel>();

    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.camera),
                title: const Text('Camera'),
                onTap: () {
                  setState(() {
                    _source = ImageSource.camera;
                  });
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const Icon(Icons.add_photo_alternate),
                title: const Text('Gallery'),
                onTap: () {
                  setState(() {
                    _source = ImageSource.gallery;
                  });
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const Icon(Icons.cancel),
                title: const Text('None'),
                onTap: () {
                  _source = null;
                  vm.reset();
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<PhotoPickerViewModel>();

    return Center(
        child: GestureDetector(
      onTap: () async {
        await _showImageSourceOptions(context);

        if (_source == null) return;

        XFile? result = await _picker.pickImage(source: _source!);

        if (result != null) {
          await vm.set(result);
        }
      },
      child: Consumer<PhotoPickerViewModel>(
        builder: (context, image, child) => CircleAvatar(
            radius: 120,
            backgroundImage: image.bytes != null
                ? MemoryImage(image.bytes!)
                : const NetworkImage('https://via.placeholder.com/150')
                    as ImageProvider),
      ),
    ));
  }
}
