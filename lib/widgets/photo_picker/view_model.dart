import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PhotoPickerViewModel extends ChangeNotifier {
  Uint8List? _bytes;
  String? _path;

  Uint8List? get bytes => _bytes;
  String? get path => _path;

  PhotoPickerViewModel() {
    _bytes = null;
    _path = null;
  }

  Future<void> set(XFile ximg) async {
    _bytes = await ximg.readAsBytes();
    _path = ximg.path;
    notifyListeners();
  }

  void reset() {
    _bytes = null;
    _path = null;
    notifyListeners();
  }
}
