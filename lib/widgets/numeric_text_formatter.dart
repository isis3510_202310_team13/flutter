import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class NumericTextFormatter extends TextInputFormatter {
  final RegExp _regex = RegExp(r'[^\d]');

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > 7) {
      return oldValue;
    }
    String newString = newValue.text.replaceAll(_regex, '');
    int value = int.tryParse(newString) ?? 0;
    String formatted =
        NumberFormat.currency(locale: 'es_CO', symbol: '', decimalDigits: 0)
            .format(value);

    return TextEditingValue(
      text: formatted.trim(),
      selection: TextSelection.collapsed(offset: formatted.trim().length),
    );
  }
}
