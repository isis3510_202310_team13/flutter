abstract class Repository<T> {
  Future<List<T>> readAll();
  Future<T> read(String ref);
  Future<T> create(T object);
  Future<void> update(String ref, T object);
  Future<void> delete(String ref);
}
