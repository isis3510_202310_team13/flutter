import 'package:project/models/client.dart';
import 'package:project/services/firebase/firestore/skeleton.dart';
import 'package:project/repositories/generic_repository.dart';

const collectionPath = 'clients';

class ClientRemoteRepository implements Repository<Client> {
  @override
  Future<List<Client>> readAll() {
    return getColl(collectionPath)
        .then((rawColl) => rawColl.map((rawDoc) => Client.fromJson(rawDoc)))
        .then((coll) => coll.toList());
  }

  @override
  Future<Client> read(String ref) {
    return getDoc(collectionPath, ref).then((raw) => Client.fromJson(raw));
  }

  @override
  Future<Client> create(Client object) {
    return addDoc(collectionPath, object.toJson())
        .then((raw) => Client.fromJson(raw));
  }

  Future<Client> createWithId(String ref, Client object) {
    return addDocWithId(collectionPath, ref, object.toJson())
        .then((value) => read(ref));
  }

  @override
  Future<void> update(String ref, Client object) {
    return updateDoc(collectionPath, ref, object.toJson());
  }

  @override
  Future<void> delete(String ref) {
    return deleteDoc(collectionPath, ref);
  }
}
