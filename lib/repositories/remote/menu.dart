import 'package:project/models/menu.dart';
import 'package:project/repositories/generic_repository.dart';
import 'package:project/services/firebase/firestore/skeleton.dart';

const collectionPath = 'menus';

class MenuRemoteRepository extends Repository<Menu> {
  @override
  Future<List<Menu>> readAll() {
    return getColl(collectionPath)
        .then((rawColl) => rawColl.map((rawDoc) => Menu.fromJson(rawDoc)))
        .then((coll) => coll.toList());
  }

  @override
  Future<Menu> read(String ref) {
    return getDoc(collectionPath, ref).then((raw) => Menu.fromJson(raw));
  }

  @override
  Future<Menu> create(Menu object) {
    return addDoc(collectionPath, object.toJson())
        .then((raw) => Menu.fromJson(raw));
  }

  @override
  Future<void> update(String ref, Menu object) {
    return updateDoc(collectionPath, ref, object.toJson());
  }

  @override
  Future<void> delete(String ref) {
    return deleteDoc(collectionPath, ref);
  }
}
