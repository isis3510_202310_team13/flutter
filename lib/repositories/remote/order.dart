import 'package:project/models/order.dart';
import 'package:project/repositories/generic_repository.dart';
import 'package:project/services/firebase/firestore/skeleton.dart';

const collectionPath = 'orders';

class OrderRemoteRepository extends Repository<Order> {
  @override
  Future<List<Order>> readAll() {
    return getColl(collectionPath)
        .then((rawColl) => rawColl.map((rawDoc) => Order.fromJson(rawDoc)))
        .then((coll) => coll.toList());
  }

  @override
  Future<Order> read(String ref) {
    return getDoc(collectionPath, ref).then((raw) => Order.fromJson(raw));
  }

  @override
  Future<Order> create(Order object) {
    return addDoc(collectionPath, object.toJson())
        .then((raw) => Order.fromJson(raw));
  }

  @override
  Future<void> update(String ref, Order object) {
    return updateDoc(collectionPath, ref, object.toJson());
  }

  @override
  Future<void> delete(String ref) {
    return deleteDoc(collectionPath, ref);
  }
}
