import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'ui_constants.dart';

class AppTheme {
  static final ThemeData lightTheme = ThemeData.light().copyWith(
    scaffoldBackgroundColor: kWhiteBackground,
    colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.orange),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        textStyle: MaterialStateProperty.all<TextStyle>(
          GoogleFonts.roboto(
            textStyle: const TextStyle(
              color: kWhite,
              fontWeight: FontWeight.w500,
              fontSize: 16,
            ),
          ),
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all(kWhite),
          textStyle: MaterialStateProperty.all<TextStyle>(GoogleFonts.roboto(
            textStyle: GoogleFonts.montserrat(
              textStyle: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ))),
    ),
    appBarTheme: AppBarTheme(
      toolbarHeight: 120,
      centerTitle: true,
      elevation: 0,
      color: Colors.transparent,
      iconTheme: const IconThemeData(color: Colors.black),
      titleTextStyle: GoogleFonts.montserrat(
        textStyle: const TextStyle(
          color: kDarkGray,
          fontSize: 24,
          fontWeight: FontWeight.w500,
        ),
      ),
      toolbarTextStyle: GoogleFonts.openSans(
        textStyle: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.normal,
        ),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: GoogleFonts.openSans(
        textStyle: const TextStyle(
          color: kgray,
          fontSize: 14,
          fontWeight: FontWeight.w800,
        ),
      ),
      enabledBorder: const UnderlineInputBorder(
        borderSide: BorderSide(
          color: kDarkGray,
        ),
      ),
      focusedBorder: const UnderlineInputBorder(
        borderSide: BorderSide(
          color: kDarkGray,
        ),
      ),
    ),
  );
}
