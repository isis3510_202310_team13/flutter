import 'package:flutter/material.dart';

const kDarkGray = Color.fromRGBO(52, 52, 52, 1); // #343434
const kWhite = Color.fromRGBO(255, 255, 255, 1); // #FFFFFF
const kYellowMustard = Color.fromRGBO(242, 201, 76, 1); // #F2C94C
const kTomato = Color.fromRGBO(255, 99, 71, 1); //#FF6347
const kWhiteBackground = Color(0xFFF6F6F9);
const kgray = Color(0xFF8D8D8D);
const kPurple = Color.fromRGBO(53, 125, 237, 1);
// const kPurple = Color.fromRGBO(235, 101, 52, 1);

// const kPurple = Color.fromRGBO(0, 166, 118, 1);
