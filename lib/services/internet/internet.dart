import 'package:connectivity_plus/connectivity_plus.dart';

Future<bool> hasInternet() async {
  final c = Connectivity();
  final res = await c.checkConnectivity();
  return res != ConnectivityResult.none;
}
