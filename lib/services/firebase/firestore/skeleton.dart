import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:project/services/firebase/firestore/singleton.dart';

FirebaseFirestoreSingleton firestore = FirebaseFirestoreSingleton();

Future<Iterable<Map<String, dynamic>>> getColl(String collPath) {
  final collRef = firestore.ref.collection(collPath);
  return collRef
      .get()
      .then((event) => event.docs.map((element) => element.data()));
}

Future<Map<String, dynamic>> getDoc(String collPath, String ref) {
  final docRef = firestore.ref.collection(collPath).doc(ref);
  return docRef.get().then((DocumentSnapshot doc) {
    final data = doc.data() as Map<String, dynamic>;
    data['ref'] = doc.id;
    return data;
  });
}

Future<Map<String, dynamic>> addDoc(
    String collPath, Map<String, dynamic> body) {
  final collRef = firestore.ref.collection(collPath);
  return collRef
      .add(body)
      .then((docRef) => docRef.get())
      .then((DocumentSnapshot doc) {
    final data = doc.data() as Map<String, dynamic>;
    data['ref'] = doc.id;
    return data;
  });
}

Future<void> addDocWithId(
    String collPath, String ref, Map<String, dynamic> body) {
  final docRef = firestore.ref.collection(collPath).doc(ref);
  return docRef.set(body);
}

Future<void> updateDoc(String collPath, String ref, Map<String, dynamic> body) {
  final docRef = firestore.ref.collection(collPath).doc(ref);
  return docRef.update(body);
}

Future<void> deleteDoc(String collPath, String ref) {
  final docRef = firestore.ref.collection(collPath).doc(ref);
  return docRef.delete();
}
