import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseFirestoreSingleton {
  static final FirebaseFirestoreSingleton _singleton =
      FirebaseFirestoreSingleton._internal();

  late FirebaseFirestore _ref;

  factory FirebaseFirestoreSingleton() {
    return _singleton;
  }

  FirebaseFirestoreSingleton._internal() {
    _ref = FirebaseFirestore.instance;
  }

  FirebaseFirestore get ref => _ref;
}
