import 'package:firebase_auth/firebase_auth.dart';
import 'package:fpdart/fpdart.dart';

FirebaseAuth auth = FirebaseAuth.instance;

String _signUpErrorMessage(Object o, StackTrace s) {
  if (o is FirebaseAuthException) {
    if (o.code == 'weak-password') {
      return 'Password is too weak!';
    } else if (o.code == 'invalid-email') {
      return 'E-mail address is not valid!';
    } else if (o.code == 'email-already-in-use') {
      return 'Email is already in use!';
    } else if (o.code == 'operation-not-allowed') {
      return 'Passwords and accounts are not enabled!';
    }
  }

  return o.toString();
}

String _signInErrorMessage(Object o, StackTrace s) {
  if (o is FirebaseAuthException) {
    if (o.code == 'invalid-email') {
      return 'E-mail is invalid!';
    } else if (o.code == 'user-disabled') {
      return 'User account is disabled!';
    } else if (o.code == 'user-not-found') {
      return 'No user found with that e-mail!';
    } else if (o.code == 'wrong-password') {
      return 'Supplied password is wrong!';
    }
  }

  return o.toString();
}

TaskEither<String, UserCredential> signUp(String email, String password) {
  return TaskEither.tryCatch(
      () =>
          auth.createUserWithEmailAndPassword(email: email, password: password),
      _signUpErrorMessage);
}

TaskEither<String, UserCredential> signIn(String email, String password) {
  return TaskEither<String, UserCredential>.tryCatch(
      () => auth.signInWithEmailAndPassword(email: email, password: password),
      _signInErrorMessage);
}
