import 'dart:typed_data';

import 'package:project/services/firebase/storage/singleton.dart';

final storageRef = FirebaseStorageSingleton().instance.ref();

Future<String> _uploadImage(String folder, String name, Uint8List bytes) async {
  final folderRef = storageRef.child('$folder/$name');
  return folderRef.putData(bytes).then((_) => folderRef.getDownloadURL());
}

Future<String> uploadProfilePhoto(String name, Uint8List bytes) async {
  return _uploadImage('profile_photos', name, bytes);
}

Future<String> uploadMenuPhoto(String name, Uint8List bytes) async {
  return _uploadImage('menu_photos', name, bytes);
}
