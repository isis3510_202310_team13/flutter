import 'package:firebase_storage/firebase_storage.dart';

class FirebaseStorageSingleton {
  static final FirebaseStorageSingleton _singleton =
      FirebaseStorageSingleton._internal();

  late FirebaseStorage _instance;

  factory FirebaseStorageSingleton() {
    return _singleton;
  }

  FirebaseStorageSingleton._internal() {
    _instance = FirebaseStorage.instance;
  }

  FirebaseStorage get instance => _instance;
}
