import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

class ConnectivityProvider with ChangeNotifier {
  late bool hasInternet;

  ConnectivityProvider(bool initsWithInternet) {
    hasInternet = initsWithInternet;
    Connectivity connectivity = Connectivity();
    connectivity.onConnectivityChanged.listen((result) async {
      if (result == ConnectivityResult.none) {
        hasInternet = false;
      } else {
        hasInternet = true;
      }

      notifyListeners();
    });
  }
}
