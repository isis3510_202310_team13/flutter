import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/bloc/user/bloc.dart';
import 'package:project/theme/ui_constants.dart';

class InsightScreenWrapper extends StatelessWidget {
  const InsightScreenWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => UsersBloc()..add(FetchAllUsers()),
      child: const InsightScreen(),
    );
  }
}

class InsightScreen extends StatelessWidget {
  const InsightScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return FutureBuilder(
          future: context.watch<UsersBloc>().calculateAverageOrdersRefs(),
          builder: (BuildContext context, AsyncSnapshot<List<int>> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Text("Error: ${snapshot.error}");
              } else {
                final average = snapshot.data;
                return Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        "2. Which is the average number of orders between all users?",
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleWithNumber(
                          metric: average![0],
                          descripcionDetailed: "with all users",
                          color: kTomato,
                        ),
                        CircleWithNumber(
                            metric: average[1],
                            descripcionDetailed:
                                "with users with at least one order",
                            color: kDarkGray),
                      ],
                    )
                  ],
                );
              }
            } else {
              return const Text("Error");
            }
          },
        );
      },
    );
  }
}

class CircleWithNumber extends StatelessWidget {
  final int metric;
  final Color color;
  final String descripcionDetailed;
  const CircleWithNumber(
      {super.key,
      required this.metric,
      required this.descripcionDetailed,
      required this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: 150,
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(100)),
      child: Column(children: [
        const SizedBox(height: 37),
        const Center(
            child: Text(
          "Average orders:",
          style: TextStyle(color: kWhite),
        )),
        Center(
          child: Text(
            metric.toString(),
            style: const TextStyle(
                fontSize: 32, fontWeight: FontWeight.w900, color: kWhite),
          ),
        ),
        Center(
            child: Text(
          descripcionDetailed,
          style: const TextStyle(
              fontSize: 10,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.italic,
              color: kWhite),
          textAlign: TextAlign.center,
        ))
      ]),
    );
  }
}
