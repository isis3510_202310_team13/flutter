import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/bloc/order/bloc.dart';
import 'package:project/features/screens.dart';

class DayOrderWrapper extends StatelessWidget {
  const DayOrderWrapper({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => OrdersBloc(),
      child: SingleChildScrollView(
        child: Column(
          children: const [DayOrderScreen(), InsightScreenWrapper()],
        ),
      ),
    );
  }
}

class DayOrderScreen extends StatefulWidget {
  const DayOrderScreen({Key? key}) : super(key: key);

  @override
  State<DayOrderScreen> createState() => _DayOrderScreenState();
}

class _DayOrderScreenState extends State<DayOrderScreen> {
  @override
  void initState() {
    super.initState();
    context.read<OrdersBloc>().add(FetchOrders());
  }

  @override
  Widget build(BuildContext context) {
    List<DayData> dataPlot = [
      DayData('Monday', 0),
      DayData('Tuesday', 0),
      DayData('Wednesday', 0),
      DayData('Thursday', 0),
      DayData('Friday', 0),
    ];

    return SizedBox(
      height: MediaQuery.of(context).size.height * .5,
      child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child:
              BlocBuilder<OrdersBloc, OrdersState>(builder: (context, state) {
            if (state is OrdersLoadedState) {
              //List<DayData> data = [];
              final year = DateTime.now().year;
              for (int i = 0; i < state.orders.length; i++) {
                final date = DateTime(year, state.orders[i].deliverMonth,
                    state.orders[i].deliverDay);
                final weekday = date.weekday - 1;
                if (weekday < 5) {
                  dataPlot[weekday].data = dataPlot[weekday].data + 1;
                }
              }
              return barPlot(dataPlot);
            } else if (state is OrdersLoadingState) {
              return const FractionallySizedBox(
                  widthFactor: 0.1,
                  heightFactor: 0.1,
                  child: CircularProgressIndicator());
            }
            return const Text('Fatal failure');
          })),
    );
  }
}

Widget barPlot(List<DayData> data) {
  return Container(
      child: Padding(
    padding: const EdgeInsets.all(16.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        AppBar(
          title: const Text('Business questions'),
        ),
        const Text(
            'What are the most common days of the week to order lunch from the cafeteria?'),
        SizedBox(
            height: 250.0,
            child: charts.BarChart([
              charts.Series(
                id: 'id',
                data: data,
                domainFn: (dayData, _) => dayData.day,
                measureFn: (dayData, _) => dayData.data,
              )
            ])),
      ],
    ),
  ));
}

class DayData {
  final String day;
  int data;

  DayData(this.day, this.data);
}
