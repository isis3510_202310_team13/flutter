export 'package:project/features/create_menu/create_menu_screen.dart';
export 'package:project/features/insights/insights_screen.dart';
export 'package:project/features/login/login_screen.dart';
export 'package:project/features/profile/profile_screen.dart';
export 'package:project/features/principal/screen.dart';
export 'package:project/features/no_internet.dart';
export 'package:project/features/screen_wrapper/screen_wrapper.dart';
export 'package:project/features/onboarding/screen.dart';
