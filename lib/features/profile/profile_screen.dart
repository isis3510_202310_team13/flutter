import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/bloc/user/bloc.dart';
import 'package:project/features/principal/widgets/checkbox_card.dart';
import 'package:project/features/principal/widgets/information_card.dart';
import 'package:project/features/screens.dart';

class ProfileScreenWrapper extends StatelessWidget {
  const ProfileScreenWrapper({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => UsersBloc(), child: const ProfileScreen());
  }
}

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final ScrollController controller = ScrollController();

  @override
  void initState() {
    super.initState();
    context.read<UsersBloc>().add(FetchUsers());
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Scaffold(body: SizedBox(
          child: BlocBuilder<UsersBloc, UsersState>(builder: (context, state) {
        if (state is UserLoadedState) {
          print(state.users.birthday);
          return Scrollbar(
              controller: controller,
              child: ListView.builder(
                  itemCount: 1,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 15, left: 20),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              Text('Information',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ))
                            ]),
                      ),
                      const Padding(
                        padding: EdgeInsets.all(10),
                        child: InformationCard(
                            name: 'Juan Perez', email: 'juanperez@yahoo.com'),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15, left: 20),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              Text('Preferences',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold))
                            ]),
                      ),
                      const Padding(
                          padding: EdgeInsets.all(10),
                          child: CheckBoxScreen(classes: [
                            'All types of food',
                            'Vegetarian',
                            'Vegan',
                            'Pescetarian'
                          ])),
                      Padding(
                        padding: const EdgeInsets.only(top: 15, left: 20),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              Text('Payment Method',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold))
                            ]),
                      ),
                      const Padding(
                          padding: EdgeInsets.all(10),
                          child: CheckBoxScreen(classes: ['Card', 'PSE'])),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                fixedSize: const Size(200, 10),
                                shape: const StadiumBorder()),
                            onPressed: () {
                              setState(() {
                                const PrincipalScreen();
                              });
                            },
                            child: const Text('Update')),
                      )
                    ]);
                  }));
        } else if (state is UsersLoadingState) {
          return const FractionallySizedBox(
              widthFactor: 0.1,
              heightFactor: 0.1,
              child: CircularProgressIndicator());
        }
        return const Text('Fatal failure');
      })));
    });
  }

  /**
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder:  (BuildContext context, BoxConstraints constraints) {
      return Scaffold(
        body: SizedBox(
          child: Scrollbar(
            controller: controller,
            child: ListView.builder(
              itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return  Column(
                  children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 15, left: 20),
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [Text('Information', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,))]),
                  ), 
                  const Padding( 
                    padding: EdgeInsets.all(10),
                    child: InformationCard1(name: 'Juan Perez', email: 'juanperez@yahoo.com'),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 15, left: 20),
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, children:[Text('Preferences', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))]),
                  ), 
                  const Padding( 
                    padding: EdgeInsets.all(10),
                    child: CheckBoxScreen(classes: ['All types of food','Vegetarian', 'Vegan','Pescetarian'])
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 15, left: 20),
                    child: Row(mainAxisAlignment:MainAxisAlignment.start, children:[Text('Payment Method', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))]),
                  ), 
                  const Padding( 
                    padding: EdgeInsets.all(10),
                    child: CheckBoxScreen(classes: ['Card','PSE'])
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        fixedSize: const Size(200, 10),
                        shape: const StadiumBorder()
                      ),
                      onPressed: () {
                        setState(() {
                          const PrincipalScreen();
                        });
                      },
                    child: const Text('Update')
                    ),
                  )
                 ]
                );
              }
            )
          )
        )
      );
    });
  } */
}
