import 'package:flutter/material.dart';
import 'package:project/features/insights/days_most_ordered.dart';
import 'package:project/features/screens.dart';
import 'package:project/theme/ui_constants.dart';

class ScreenWrapper extends StatefulWidget {
  const ScreenWrapper({super.key});

  @override
  State<ScreenWrapper> createState() => _ScreenWrapperState();
}

List<Widget> _options = [
  const PrincipalScreen(),
  const ProfileScreenWrapper(),
  const CreateMenuWrapper(),
  const DayOrderWrapper(),
];

List<String> _titles = [
  'Welcome!',
  'My profile',
  'Create menu',
  'Analytics view'
];

class _ScreenWrapperState extends State<ScreenWrapper> {
  int _selectedIndex = 0;

  void _changeIndex(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.home),
            onPressed: () {
              if (_selectedIndex != 0) {
                _changeIndex(0);
              }
            },
          ),
          title: Text(_titles.elementAt(_selectedIndex)),
        ),
        body: Container(
          constraints: const BoxConstraints.expand(),
          child: _options.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: _navBarItems(),
          currentIndex: _selectedIndex,
          onTap: _changeIndex,
        ));
  }
}

List<BottomNavigationBarItem> _navBarItems() {
  return const [
    BottomNavigationBarItem(
        icon: Icon(
          Icons.home,
          color: kDarkGray,
        ),
        label: ''),
    BottomNavigationBarItem(
        icon: Icon(
          Icons.account_circle_outlined,
          color: kDarkGray,
        ),
        label: ''),
    BottomNavigationBarItem(
        icon: Icon(
          Icons.add,
          color: kDarkGray,
        ),
        label: ''),
    BottomNavigationBarItem(
        icon: Icon(
          Icons.article,
          color: kDarkGray,
        ),
        label: ''),
  ];
}
