import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project/bloc/menu/bloc.dart';
import 'package:project/services/notifications_service.dart';
import 'package:project/widgets/button_tomato.dart';
import 'package:project/widgets/input_decorations.dart';
import 'package:project/widgets/numeric_text_formatter.dart';

import 'package:project/widgets/photo_picker/photo_picker.dart';
import 'package:project/widgets/photo_picker/view_model.dart';
import 'package:provider/provider.dart';

class CreateMenuWrapper extends StatelessWidget {
  const CreateMenuWrapper({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MenusBloc(),
      child: ChangeNotifierProvider(
        create: (context) => PhotoPickerViewModel(),
        child: const CreateMenu(),
      ),
    );
  }
}

class CreateMenu extends StatefulWidget {
  const CreateMenu({super.key});

  @override
  State<CreateMenu> createState() => _CreateMenuState();
}

class _CreateMenuState extends State<CreateMenu> {
  String _description = "";
  String _name = "";
  int _price = 0;
  bool _isFocusedDescription = false;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Padding(
      padding: const EdgeInsets.only(left: 50, right: 50),
      child: Column(
        children: [
          const OnboardingPhotoPicker(),
          const SizedBox(
            height: 25,
          ),
          Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(children: [
              TextFormField(
                  onChanged: (value) => _name = value,
                  onTap: () {
                    setState(() {
                      _isFocusedDescription = false;
                    });
                  },
                  autofocus: true,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(35),
                  ],
                  keyboardType: TextInputType.name,
                  decoration: InputDecorations.authInputDecorations(
                      labelText: "Name",
                      prefixIcon: Icons.bakery_dining_rounded),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "The name cannot be empty";
                    }
                    if (value.length <= 2) {
                      return "The name must be at least 2 characters";
                    }
                    return null;
                  }),
              TextFormField(
                maxLines: null,
                maxLength: 150,
                onChanged: (value) {
                  setState(() {
                    _description = value;
                    _isFocusedDescription = _description.isEmpty ? false : true;
                  });
                },
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.wysiwyg),
                  labelText: 'Description',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w800,
                    color: _isFocusedDescription
                        ? const Color.fromARGB(255, 255, 153, 0)
                        : Colors.grey,
                  ),
                  counterText: '${_description.length}/150',
                ),
                onTap: () {
                  setState(() {
                    _isFocusedDescription = true;
                  });
                },
                onEditingComplete: () {
                  setState(() {
                    _isFocusedDescription = _description.isEmpty ? false : true;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a description';
                  }
                  return null;
                },
              ),
              TextFormField(
                  onChanged: (value) {
                    _price = int.parse(value.replaceAll('.', ''));
                  },
                  onTap: () {
                    setState(() {
                      _isFocusedDescription = false;
                    });
                  },
                  autofocus: true,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(8),
                    NumericTextFormatter()
                  ],
                  keyboardType: TextInputType.number,
                  decoration: InputDecorations.authInputDecorations(
                    labelText: "Price",
                    prefixIcon: Icons.attach_money,
                  ),
                  validator: (value) {
                    if (value == "0") {
                      return "The price cannot be 0";
                    }
                    if (_price <= 99) {
                      return "The price cannot be less than 100";
                    }
                    return null;
                  }),
              const SizedBox(
                height: 55,
              ),
              ButtonTomato(
                text: "Create",
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  if (!_formKey.currentState!.validate()) {
                    NotificationsService.showSnackbar(
                        "Please enter a valid state for the form");
                    return;
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: ListTile(
                          leading: CircularProgressIndicator(),
                          title: Text('The new menu is almost ready')),
                    ));
                    final image = context.read<PhotoPickerViewModel>();
                    if (image.bytes == null) {
                      NotificationsService.showSnackbar("Menu without image");
                    }
                    BlocProvider.of<MenusBloc>(context).add(CreateNewMenu(
                      name: _name,
                      description: _description,
                      price: _price,
                      image: image.bytes!,
                    ));
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: ListTile(
                          leading: CircularProgressIndicator(),
                          title: Text('The new menu is ready! :D')),
                    ));
                  }
                },
              ),
            ]),
          ),
        ],
      ),
    ));
  }
}
