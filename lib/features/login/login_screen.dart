import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project/providers/login_form_provider.dart';
import 'package:project/services/firebase/auth/service.dart';
import 'package:project/services/notifications_service.dart';
import 'package:project/theme/ui_constants.dart';
import 'package:project/widgets/input_decorations.dart';
import '../../widgets/widgets.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Center(
      child: AuthBackground(
        childForm: Stack(children: [
          Column(children: [
            const SizedBox(
              height: 35,
            ),
            Container(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                width: double.infinity,
                child: ChangeNotifierProvider(
                    create: (_) => LoginFormProvider(),
                    child: const LoginForm())),
          ]),
        ]),
      ),
    )));
  }
}

class LoginForm extends StatelessWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    final loginform = Provider.of<LoginFormProvider>(context);

    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: loginform.formKey,
      child: Column(
        children: [
          TextFormField(
            autocorrect: false,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecorations.authInputDecorations(
                labelText: "Email address"),
            inputFormatters: [
              LengthLimitingTextInputFormatter(65),
            ],
            onChanged: (value) => loginform.email = value,
            validator: (value) {
              String pattern =
                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

              RegExp regExp = RegExp(pattern);

              return regExp.hasMatch(value ?? "")
                  ? null
                  : "It doesn't look like an email D:";
            },
          ),
          const SizedBox(
            height: 46,
          ),
          TextFormField(
            autocorrect: false,
            inputFormatters: [
              LengthLimitingTextInputFormatter(35),
            ],
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
            decoration:
                InputDecorations.authInputDecorations(labelText: "Password"),
            onChanged: (value) => loginform.password = value,
            validator: (value) {
              if (value != null) {}
            },
          ),
          const SizedBox(height: 34),
          TextButton(
              onPressed: () {},
              child: const Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Forgot password?",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: kTomato),
                ),
              )),
          const SizedBox(height: 60),
          ButtonTomato(
              text: loginform.isLoading ? "Wait..." : "Log-in",
              onPressed: loginform.isLoading
                  ? null
                  : () async {
                      FocusScope.of(context).unfocus();
                      if (!loginform.isValidForm()) return;
                      loginform.isLoading = true;

                      final response =
                          await signIn(loginform.email, loginform.password)
                              .run();
                      final user = response.fold(
                        (error) {
                          NotificationsService.showSnackbar(error);
                        },
                        (userCredential) {
                          return userCredential.user;
                        },
                      );

                      if (user != null) {
                        await Future.delayed(const Duration(seconds: 2));

                        loginform.isLoading = false;
                        Navigator.restorablePushReplacementNamed(
                            context, "home");
                      } else {
                        loginform.isLoading = false;
                      }
                    }),
          const SizedBox(
            height: 20,
          ),
          TextButton(
              onPressed: () {
                Navigator.restorablePushReplacementNamed(context, "signUp");
              },
              child: const Align(
                alignment: Alignment.center,
                child: Text(
                  "Don't have an account yet? \n create one! ",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: kTomato),
                ),
              )),
        ],
      ),
    );
  }
}
