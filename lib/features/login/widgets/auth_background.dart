import 'package:flutter/material.dart';

class AuthBackground extends StatelessWidget {
  final Widget childForm;
  const AuthBackground({Key? key, required this.childForm});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [_Logo(), childForm],
    );
  }
}

class _Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.width,
        height: size.height * 0.35,
        decoration: _createCardShadow(),
        child: const Image(
            image: AssetImage("assets/images/logo_cafeteria.png"),
            fit: BoxFit.scaleDown));
  }

  BoxDecoration _createCardShadow() {
    return const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(25)),
        boxShadow: [
          BoxShadow(color: Colors.black38, blurRadius: 15, offset: Offset(0, 5))
        ]);
  }
}
