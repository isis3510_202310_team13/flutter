import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project/theme/ui_constants.dart';

class NoInternetScreen extends StatelessWidget {
  const NoInternetScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Oops...'),
          automaticallyImplyLeading: false,
        ),
        body: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          const Icon(
            Icons.wifi_off,
            size: 200,
            color: kDarkGray,
          ),
          Text(
            'No internet connection!\nPlease refresh',
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
                fontSize: 20, fontWeight: FontWeight.w600, color: kDarkGray),
          )
        ])));
  }
}
