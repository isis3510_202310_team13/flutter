import 'package:flutter/material.dart';

class InformationCard extends StatelessWidget {
  final String name;
  final String email;
  final String? alergies;

  const InformationCard({
    super.key,
    required this.name,
    required this.email,
    this.alergies,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Row(
      children: [
        Column(children: const [
          Padding(
            padding: EdgeInsets.all(15),
            child: Image(
              image: AssetImage('assets/images/user.png'),
              width: 110,
              height: 110,
            ),
          )
        ]),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                const SizedBox(height: 5),
                TextField(
                  decoration: InputDecoration(hintText: email),
                  enabled: false,
                ),
                const SizedBox(height: 15),
                const Text(
                  'Alergies',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
                const SizedBox(height: 5),
                TextField(
                  decoration: InputDecoration(
                    hintText: alergies ?? 'None',
                  ),
                  enabled: false,
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
