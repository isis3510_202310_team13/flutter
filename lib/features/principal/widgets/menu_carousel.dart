import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project/bloc/menu/bloc.dart';
import 'package:project/models/menu.dart';
import 'package:project/theme/ui_constants.dart';

class MenuCarouselWrapper extends StatelessWidget {
  const MenuCarouselWrapper({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MenusBloc(),
      child: const MenuCarousel(),
    );
  }
}

class MenuCarousel extends StatefulWidget {
  const MenuCarousel({Key? key}) : super(key: key);

  @override
  State<MenuCarousel> createState() => _MenuCarouselState();
}

class _MenuCarouselState extends State<MenuCarousel> {
  late int _activePage;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(viewportFraction: 0.8, initialPage: 0);
    _activePage = 0;
    context.read<MenusBloc>().add(FetchMenus());
  }

  @override
  Widget build(BuildContext context) {
    final windowWidth = MediaQuery.of(context).size.width;

    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: SizedBox(
            width: windowWidth,
            height: 500,
            child:
                BlocBuilder<MenusBloc, MenusState>(builder: (context, state) {
              if (state is MenusLoadedState) {
                return _carousel(state.menus);
              } else if (state is MenusLoadingState) {
                return const FractionallySizedBox(
                    widthFactor: 0.1,
                    heightFactor: 0.1,
                    child: CircularProgressIndicator());
              }

              // state is MenusErrorState
              return const Text('Fatal failure');
            })));
  }

  // This widget is dependent on the state of the widget
  // so it has to be put inside
  Widget _carousel(List<Menu> menus) => PageView.builder(
      itemCount: menus.length,
      pageSnapping: true,
      controller: _pageController,
      onPageChanged: (page) {
        setState(() => _activePage = page);
      },
      itemBuilder: (context, pagePosition) {
        bool active = pagePosition == _activePage;
        return _slider(menus, pagePosition, active);
      });
}

AnimatedContainer _slider(List<Menu> menus, int pagePosition, bool active) {
  double margin = active ? 10 : 20;

  return AnimatedContainer(
    duration: const Duration(milliseconds: 500),
    curve: Curves.easeInOutCubic,
    margin: EdgeInsets.all(margin),
    child: _menuCard(menus[pagePosition]),
  );
}

Widget _menuCard(Menu menu) {
  return Card(
    elevation: 0,
    child: Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(menu.photoURL), //NetworkImage
            radius: 100,
          ), //CircleAvatar
          const SizedBox(
            height: 10,
          ), //SizedBox
          Text(
            menu.name,
            style: GoogleFonts.montserrat(
              fontSize: 20,
              color: kDarkGray,
              fontWeight: FontWeight.w400,
            ),
          ), //Textstyle //Text
          const SizedBox(
            height: 20,
          ), //SizedBox
          Text(
            menu.description,
            style: const TextStyle(
              fontSize: 15,
              color: kDarkGray,
            ), //Textstyle
          ), //Text
          const Spacer(), //SizedBox
          Row(children: [
            Text(
              '${menu.price.toString()} COP',
              style: GoogleFonts.montserrat(
                fontSize: 15,
                color: Colors.orange,
                fontWeight: FontWeight.bold,
              ),
            ),
            const Spacer(),
            ElevatedButton(
                onPressed: () => 'Null',
                child: Text('Order',
                    style: GoogleFonts.montserrat(
                        fontSize: 10,
                        color: Colors.white,
                        fontWeight: FontWeight.bold)))
          ]),
          //SizedBox
        ],
      ), //Column
    ), //Padding
    //SizedBox
  );
}
