import 'package:flutter/material.dart';

class CheckBoxScreen extends StatefulWidget {
   
  final List<String> classes;
  const CheckBoxScreen({Key? key, required this.classes}) : super(key: key);

  @override
  State<CheckBoxScreen> createState() => _CheckBoxScreenState();
}

class _CheckBoxScreenState extends State<CheckBoxScreen> {

  @override
  Widget build(BuildContext context) {
    
    String? selectedOption;

    return Card(
      child: Column(
      children:
        widget.classes.map((option) => RadioListTile(
          title: Text(option),
          value: option,
          groupValue: selectedOption,
          onChanged: (value){
              setState(() {
                selectedOption = value;
              });
          })
          ).toList(),
    ));
  }
}