import 'package:flutter/material.dart';
import 'package:project/features/principal/widgets/menu_carousel.dart';

class PrincipalScreen extends StatelessWidget {
  const PrincipalScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [MenuCarouselWrapper()],
    );
  }
}
