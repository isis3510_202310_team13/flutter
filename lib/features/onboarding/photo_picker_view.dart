import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import 'package:project/models/client.dart';
import 'package:project/repositories/remote/client.dart';
import 'package:project/services/firebase/auth/service.dart';
import 'package:project/services/firebase/storage/service.dart';
import 'package:project/widgets/photo_picker/photo_picker.dart';
import 'package:project/widgets/photo_picker/view_model.dart';
import 'package:provider/provider.dart';

TaskEither<String, void> _finishOnboarding(String name, String email,
    String password, Client client, Uint8List? photoBytes) {
  return signUp(email, password)
      .flatMap((credential) => TaskEither.tryCatch(() async {
            final user = credential.user!;

            // Register user in our database
            final clientRepo = ClientRemoteRepository();
            await clientRepo.createWithId(user.uid, client);

            // Update name of the user
            await user.updateDisplayName(name);

            // Get url of the user's photo
            if (photoBytes != null) {
              final timestamp =
                  DateTime.now().millisecondsSinceEpoch.toString();
              final photoUrl = await uploadProfilePhoto(timestamp, photoBytes);
              await user.updatePhotoURL(photoUrl);
            }
          }, (error, stackTrace) => error.toString()));
}

Future<void> _showFinishDialog(
    BuildContext context, Either<String, void> response) {
  final status = response.isRight();
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            title: const Text('Just that you know...'),
            content: Text(
                response.fold((l) => l, (r) => 'Profile created sucessfully')),
            actions: [
              TextButton(
                  child: Text(status ? 'Ok' : 'Retry'),
                  onPressed: () => Navigator.pushNamed(context, 'home'))
            ],
          ));
}

class OnboardingPhotoPickerView extends StatelessWidget {
  const OnboardingPhotoPickerView(
      {super.key,
      required this.name,
      required this.email,
      required this.password,
      required this.status,
      this.date});

  final String name;
  final String email;
  final String password;
  final String status;
  final String? date;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('One final step...')),
        body: Column(children: [
          const Padding(
            padding: EdgeInsets.all(40.0),
            child: OnboardingPhotoPicker(),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: ElevatedButton(
                child: const Text('Finish'),
                onPressed: () async {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: ListTile(
                        leading: CircularProgressIndicator(),
                        title: Text('Finishing onboarding')),
                  ));

                  final image = context.read<PhotoPickerViewModel>();
                  final client = Client(birthday: date, status: status);
                  final response = await _finishOnboarding(
                          name, email, password, client, image.bytes)
                      .run();

                  if (context.mounted) {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    _showFinishDialog(context, response);
                  }
                },
              ))
        ]));
  }
}
