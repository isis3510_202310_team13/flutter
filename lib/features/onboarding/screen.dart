import 'package:flutter/material.dart';
import 'package:project/features/onboarding/widgets/form/form.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Onboarding')),
      body: const OnboardingForm(),
    );
  }
}
