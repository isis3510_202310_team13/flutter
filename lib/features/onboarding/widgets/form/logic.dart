import 'package:email_validator/email_validator.dart';
import 'package:project/utils/misc.dart';

String? validateNameField(String? value) =>
    isFieldEmpty(value).match((error) => error, (success) => null);

String? validateEmailField(String? value) {
  return isFieldEmpty(value)
      .andThen(() => holds(
          predicate: EmailValidator.validate(value!),
          errorMessage: 'Invalid email'))
      .match((error) => error, (success) => null);
}

String? validatePasswordRetype(String password, String? value) {
  return isFieldEmpty(value)
      .andThen(() => holds(
          predicate: value == password,
          errorMessage: 'Passwords don\'t match!'))
      .match((error) => error, (success) => null);
}
