import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pw_validator/Resource/Strings.dart';
import 'package:flutter_pw_validator/flutter_pw_validator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project/features/onboarding/photo_picker_view.dart';
import 'package:project/features/onboarding/widgets/form/logic.dart';
import 'package:intl/intl.dart';
import 'package:project/widgets/photo_picker/view_model.dart';
import 'package:provider/provider.dart';

class OnboardingForm extends StatefulWidget {
  const OnboardingForm({super.key});

  @override
  OnboardingFormState createState() => OnboardingFormState();
}

enum AccountType { client, staff }

class CorrectSpellings implements FlutterPwValidatorStrings {
  @override
  final String atLeast = 'At least - character(s)';

  @override
  final String uppercaseLetters = '- uppercase letter(s)';

  @override
  final String numericCharacters = '- numeric character(s)';

  @override
  final String specialCharacters = '- special character(s)';

  @override
  final String normalLetters = '- letters';
}

class OnboardingFormState extends State<OnboardingForm> {
  final _formKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _dateController = TextEditingController();

  AccountType? _accountType = AccountType.client;
  late bool _strongPassword;

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _dateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
            child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    TextFormField(
                        controller: _nameController,
                        decoration: const InputDecoration(
                            icon: Icon(Icons.face),
                            labelText: 'Name',
                            hintText: 'How do people call you?'),
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                              RegExp(r'^[ a-zA-ZÀ-ÿ\u00f1\u00d1]*$')),
                          LengthLimitingTextInputFormatter(50)
                        ],
                        validator: validateNameField),
                    TextFormField(
                        controller: _emailController,
                        decoration: const InputDecoration(
                            icon: Icon(Icons.mail),
                            labelText: 'E-mail',
                            hintText: 'Enter your e-mail'),
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        validator: validateEmailField),
                    TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(
                            icon: Icon(Icons.password),
                            labelText: 'Password',
                            hintText: 'Make sure it is safe!'),
                        inputFormatters: [LengthLimitingTextInputFormatter(20)],
                        controller: _passwordController),
                    FlutterPwValidator(
                        controller: _passwordController,
                        minLength: 6,
                        uppercaseCharCount: 2,
                        numericCharCount: 3,
                        specialCharCount: 1,
                        width: 400,
                        height: 150,
                        strings: CorrectSpellings(),
                        onSuccess: () => setState(() => _strongPassword = true),
                        onFail: () => setState(() => _strongPassword = false)),
                    TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(
                            labelText: 'Re-type your password'),
                        inputFormatters: [LengthLimitingTextInputFormatter(20)],
                        validator: (value) => validatePasswordRetype(
                            _passwordController.text, value)),
                    TextFormField(
                        controller: _dateController,
                        decoration: const InputDecoration(
                            icon: Icon(Icons.calendar_today),
                            labelText: 'Birthday date',
                            hintText:
                                'If you want it, you can leave this empty'),
                        readOnly: true,
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(1920),
                              lastDate: DateTime.now());

                          if (pickedDate != null) {
                            String formattedDate =
                                DateFormat('yyyy-MM-dd').format(pickedDate);

                            setState(
                                () => _dateController.text = formattedDate);
                          }
                        }),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Text('To what group do you belong to?',
                                textAlign: TextAlign.left,
                                style: GoogleFonts.montserrat(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[600],
                                ))),
                        ListTile(
                            title: const Text('Client'),
                            leading: Radio<AccountType>(
                              value: AccountType.client,
                              groupValue: _accountType,
                              onChanged: (AccountType? value) =>
                                  setState(() => _accountType = value),
                            )),
                        ListTile(
                          title: const Text('Staff'),
                          leading: Radio<AccountType>(
                            value: AccountType.staff,
                            groupValue: _accountType,
                            onChanged: (AccountType? value) =>
                                setState(() => _accountType = value),
                          ),
                        )
                      ],
                    ),
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate() &&
                                _strongPassword) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ChangeNotifierProvider(
                                              create: (context) =>
                                                  PhotoPickerViewModel(),
                                              child: OnboardingPhotoPickerView(
                                                  name: _nameController.text,
                                                  password:
                                                      _passwordController.text,
                                                  email: _emailController.text,
                                                  status: _accountType ==
                                                          AccountType.client
                                                      ? 'client'
                                                      : 'staff',
                                                  date: _dateController
                                                          .text.isNotEmpty
                                                      ? _dateController.text
                                                      : null))));
                            }
                          },
                          child: const Text('Next'),
                        ))
                  ],
                ))));
  }
}
