import 'package:fpdart/fpdart.dart';

// Generic predicate checking
Either<String, void> holds(
        {required bool predicate, required String errorMessage}) =>
    predicate ? Either.of(null) : Either.left(errorMessage);

Either<String, void> isFieldEmpty(String? s) => holds(
    predicate: s != null && s.isNotEmpty,
    errorMessage: 'Provided input is empty');
