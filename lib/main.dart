import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:project/providers/providers.dart';
import 'package:project/features/screens.dart';
import 'package:project/services/internet/internet.dart';
import 'package:project/services/notifications_service.dart';
import 'package:project/theme/app_theme.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    //DeviceOrientation.portraitDown,
  ]);
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  bool initsWithInternet = await hasInternet();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => ConnectivityProvider(initsWithInternet),
      ),
    ],
    child: const SpeedLunch(),
  ));
}

Widget _wrapUnconnected(BuildContext context, Widget screen) {
  return Consumer<ConnectivityProvider>(
      builder: (context, state, child) =>
          state.hasInternet ? child! : const NoInternetScreen(),
      child: screen);
}

class SpeedLunch extends StatelessWidget {
  const SpeedLunch({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Speed Lunch',
      theme: AppTheme.lightTheme,
      scaffoldMessengerKey: NotificationsService.messengerKey,
      initialRoute: 'login',
      routes: {
        'login': (context) => _wrapUnconnected(context, const LoginScreen()),
        'signUp': (context) =>
            _wrapUnconnected(context, const OnboardingScreen()),
        'home': (context) => _wrapUnconnected(context, const ScreenWrapper()),
        'newMenu': (context) =>
            _wrapUnconnected(context, const CreateMenuWrapper()),
      },
    );
  }
}
