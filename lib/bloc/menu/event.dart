part of 'bloc.dart';

abstract class MenusEvent extends Equatable {
  const MenusEvent();
}

class FetchMenus extends MenusEvent {
  @override
  List<Object> get props => [];
}

class CreateNewMenu extends MenusEvent {
  final String name;
  final String description;
  final int price;
  final Uint8List image;

  const CreateNewMenu(
      {required this.image,
      required this.name,
      required this.description,
      required this.price});

  @override
  List<Object> get props => [name, description, price, image];
}
