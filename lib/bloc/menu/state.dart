part of 'bloc.dart';

abstract class MenusState extends Equatable {}

class MenusLoadingState extends MenusState {
  @override
  List<Object?> get props => [];
}

class MenusLoadedState extends MenusState {
  final List<Menu> menus;

  MenusLoadedState(this.menus);
  @override
  List<Object?> get props => [menus];
}

class MenusErrorState extends MenusState {
  final String error;

  MenusErrorState(this.error);

  @override
  List<Object?> get props => [error];
}
