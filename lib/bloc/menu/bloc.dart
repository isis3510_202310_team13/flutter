import 'dart:typed_data';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:project/models/menu.dart';
import 'package:project/repositories/generic_repository.dart';
import 'package:project/repositories/remote/menu.dart';
import 'package:project/services/firebase/storage/service.dart';

part 'state.dart';
part 'event.dart';

class MenusBloc extends Bloc<MenusEvent, MenusState> {
  final Repository<Menu> _menuRepository = MenuRemoteRepository();

  MenusBloc() : super(MenusLoadingState()) {
    on<FetchMenus>((event, emit) async {
      List<Menu> menus = await _menuRepository.readAll();
      emit(MenusLoadedState(menus));
    });

    on<CreateNewMenu>((event, emit) async {
      var photoUrl = "";
      try {
        if (event.image != null) {
          final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
          photoUrl = await uploadMenuPhoto(timestamp, event.image);
        }
        Menu menu = Menu(
          photoURL: photoUrl,
          name: event.name,
          description: event.description,
          price: event.price,
          returnPolicy: "True",
        );
        await _menuRepository.create(menu);
        List<Menu> menus = await _menuRepository.readAll();
        emit(MenusLoadedState(menus));
      } catch (e) {
        emit(MenusErrorState(e.toString()));
      }
    });
  }
}
