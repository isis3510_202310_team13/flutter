part of 'bloc.dart';

abstract class OrdersEvent extends Equatable {
  const OrdersEvent();
}

class FetchOrders extends OrdersEvent {
  @override
  List<Object> get props => [];
}
