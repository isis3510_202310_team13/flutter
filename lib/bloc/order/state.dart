part of 'bloc.dart';

abstract class OrdersState extends Equatable {}

class OrdersLoadingState extends OrdersState {
  @override
  List<Object?> get props => [];
}

class OrdersLoadedState extends OrdersState {
  final List<Order> orders;

  OrdersLoadedState(this.orders);
  @override
  List<Object?> get props => [orders];
}

class OrdersErrorState extends OrdersState {
  final String error;

  OrdersErrorState(this.error);

  @override
  List<Object?> get props => [error];
}
