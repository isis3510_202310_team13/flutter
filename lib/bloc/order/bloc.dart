import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:project/models/order.dart';
import 'package:project/repositories/generic_repository.dart';
import 'package:project/repositories/remote/order.dart';

part 'state.dart';
part 'event.dart';

class OrdersBloc extends Bloc<OrdersEvent, OrdersState> {
  final Repository<Order> _orderRepository = OrderRemoteRepository();

  OrdersBloc() : super(OrdersLoadingState()) {
    on<FetchOrders>((event, emit) async {
      List<Order> orders = await _orderRepository.readAll();
      emit(OrdersLoadedState(orders));
    });
  }
}
