part of 'bloc.dart';

abstract class UsersEvent extends Equatable {
  const UsersEvent();
}

class FetchUsers extends UsersEvent {
  @override
  List<Object> get props => [];
}

class FetchAllUsers extends UsersEvent {
  @override
  List<Object> get props => [];
}
