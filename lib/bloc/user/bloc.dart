import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:project/models/client.dart';
import 'package:project/repositories/generic_repository.dart';
import 'package:project/repositories/remote/client.dart';

part 'state.dart';
part 'event.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  final Repository<Client> _userRepository = ClientRemoteRepository();

  UsersBloc() : super(UsersLoadingState()) {
    on<FetchUsers>((event, emit) async {
      Client user = await _userRepository.read('NpiTlynBv0g4db6ZY1iAE8WTwOO2');
      emit(UserLoadedState(user));
    });
    on<FetchAllUsers>((event, emit) async {
      List<Client> users = await _userRepository.readAll();
      emit(UsersLoadedState(users));
    });
  }

  Future<List<int>> calculateAverageOrdersRefs() async {
    final users = await _userRepository.readAll();
    final ordersRefsSum = users.fold<int>(
      0,
      (previousValue, user) =>
          previousValue +
          (user.ordersRefs == null ? 0 : user.ordersRefs!.length),
    );
    var newLen = 0;
    for (var user in users) {
      if (user.ordersRefs == null) {
        newLen++;
      }
    }
    final average = ordersRefsSum / users.length;
    final averageModified = ordersRefsSum / newLen;
    return [average.round(), averageModified.round()];
  }
}
