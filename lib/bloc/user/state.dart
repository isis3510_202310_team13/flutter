part of 'bloc.dart';

abstract class UsersState extends Equatable {}

class UsersLoadingState extends UsersState {
  @override
  List<Object?> get props => [];
}

class UserLoadedState extends UsersState {
  final Client users;

  UserLoadedState(this.users);
  @override
  List<Object?> get props => [users];
}

class UsersLoadedState extends UsersState {
  final List<Client> users;

  UsersLoadedState(this.users);
  @override
  List<Object?> get props => [users];
}

class UsersErrorState extends UsersState {
  final String error;

  UsersErrorState(this.error);

  @override
  List<Object?> get props => [error];
}
